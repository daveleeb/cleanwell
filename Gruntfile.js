/*
 * Generated on 2015-01-20
 * generator-assemble v0.5.0
 * https://github.com/assemble/generator-assemble
 *
 * Copyright (c) 2015 Hariadi Hinta
 * Licensed under the MIT license.
 */

'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// '<%= config.src %>/templates/pages/{,*/}*.hbs'
// use this if you want to match all subfolders:
// '<%= config.src %>/templates/pages/**/*.hbs'

module.exports = function(grunt) {

  require('time-grunt')(grunt);
  require('load-grunt-tasks')(grunt);

  var host = "cleanwell.at";

  var ftpConf;
  try {
      ftpConf = grunt.file.readJSON('ftp_cred.json')
  } catch (e) {
      ftpConf = {
          username: process.env.FTP_USER,
          password: process.env.FTP_PWD
      }
  }

  // Project configuration.
  grunt.initConfig({

    config: {
      src: 'src',
      dist: 'dist'
    },

    ftpush: {
      build: {
        auth: {
          host: host,
          port: 21,
          username: ftpConf.username,
          password: ftpConf.password
        },
        src: 'dist/',
        dest:  '/html/'
      }
    },

    watch: {
      assemble: {
        files: ['<%= config.src %>/{data,templates}/{,*/}*.{md,hbs,yml,css,jpg,png}', '<%= config.src %>/templates/assets/css/style.less'],
        tasks: ['assemble', 'less']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.dist %>/{,*/}*.html',
          '<%= config.dist %>/assets/{,*/}*.css',
          '<%= config.dist %>/assets/{,*/}*.js',
          '<%= config.dist %>/assets/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
          '<%= config.dist %>/event-template.html}'
        ]
      },
      css: {
        files: ['<%= config.src %>/templates/assets/css/**/*.css'],
        tasks: ['cssmin']
      },
      js: {
        files: ['<%= config.src %>/templates/assets/js/**/*.js'],
        tasks: ['concat', 'uglify']
      },
      images: {
        files: ['<%= config.src %>/templates/assets/img/**/*.{jpg,png}'],
        tasks: ['copy']
      },
      template: {
        files: ['<%= config.src %>/templates/assets/js/events/event-template.html'],
        tasks: ['copy']
      }
    },

    connect: {
      options: {
        port: 9000,
        livereload: 35729,
        // change this to '0.0.0.0' to access the server from outside
        hostname: 'localhost'
      },
      livereload: {
        options: {
          open: true,
          base: [
            '<%= config.dist %>'
          ]
        }
      }
    },

    less: {
      development: {
        option: {
          paths: ['<%= config.src %>/templates/assets/css/']
        },
        files: {
          '<%= config.src %>/templates/assets/css/style.css': '<%= config.src %>/templates/assets/css/style.less'
        }
      }
    },

    assemble: {
      pages: {
        options: {
          flatten: true,
          assets: '<%= config.dist %>/assets',
          layout: '<%= config.src %>/templates/layouts/default.hbs',
          data: '<%= config.src %>/data/*.{json,yml}',
          partials: '<%= config.src %>/templates/partials/*.hbs',
          plugins: ['assemble-contrib-sitemap', 'assemble-contrib-permalinks'],
          permalinks: {
            structure: ':directory/index.html',
            replacement: 'directory'
          },
          sitemap: {
            homepage: host,
            robot: false,
            relativedest: true,
            exclude: ['test', 'demo', 'index2']
          }
        },
        files: {
          '<%= config.dist %>/': ['<%= config.src %>/templates/pages/*.hbs']
        }
      }
    },

    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [
          '<%= config.src %>/templates/assets/js/jquery-3.6.0.min.js',
          '<%= config.src %>/templates/assets/js/bootstrap.js',
          '<%= config.src %>/templates/assets/js/custom.js',
          '<%= config.src %>/templates/assets/js/imagelightbox.min.js',
          '<%= config.src %>/templates/assets/js/typed.min.js'
        ],
        dest: '<%= config.dist %>/assets/js/script.js'
      }
    },

    cssmin: {
      dist: {
        files: {
          '<%= config.dist %>/assets/css/style.min.css': [
            '<%= config.src %>/templates/assets/css/bootstrap.css',
            '<%= config.src %>/templates/assets/css/imagelightbox.min.css',
            '<%= config.src %>/templates/assets/css/style.css'
          ]
        }
      }
    },

    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: [{
          expand: true,
          cwd: 'dist',
          src: '**/*.html',
          dest: 'dist'
        }]
      }
    },


    uglify: {
      dist: {
        files: {
          '<%= config.dist %>/assets/js/script.min.js': ['<%= config.dist %>/assets/js/script.js']
        }
      }
    },

    copy: {
      assets: {
        expand: true,
        cwd: '<%= config.src %>/templates/assets',
        src: ['css/fonts/**/*', 'img/**/*', 'data/**/*'],
        dest: '<%= config.dist %>/assets'
      },
      favicon: {
        expand: true,
        cwd: '<%= config.src %>/templates/assets/img',
        src: ['favicon.ico'],
        dest: '<%= config.dist %>/'
      },
      templates: {
        expand: true,
        cwd: '<%= config.src %>/templates/assets/js/events',
        src: ['event-template.html'],
        dest: '<%= config.dist %>/'
      },
      htaccess: {
        expand: true,
        cwd: '<%= config.src %>/templates',
        src: ['.htaccess'],
        dest: '<%= config.dist %>/'
      }
    },

    rev: {
      files: {
        src: ['<%= config.dist %>/assets/**/*.{js,css,png,jpg}']
      }
    },

    useminPrepare: {
      html: '<%= config.dist %>/{,*/}*.html'
    },

    usemin: {
      html: ['<%= config.dist %>/{,*/}*.html'],
      css: ['<%= config.dist %>/assets/css/{,*/}*.css'],
      options: {
        assetsDirs: ['<%= config.dist %>','<%= config.dist%>/assets/img']
      }
    },

    compress : {
      main : {
        options : {
          archive : "dist/template.zip"
        },
        files : [
          { expand: true, src : "**/*", cwd : "dist/" }
        ]
      }
    },

    // Before generating any new files,
    // remove any previously-created files.
    clean: ['<%= config.dist %>/**/*']

  });

  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-rev');

  grunt.registerTask('server', [
    'build',
    'htmlmin',
    'connect:livereload',
    'watch'
  ]);

  grunt.registerTask('release', [
    'build',
    'rev',
    'usemin',
    'ftpush'
  ]);

  grunt.registerTask('build',[
    'clean',
    'concat',
    'less',
    'cssmin',
    'uglify',
    'copy',
    'assemble'
  ])

  grunt.registerTask('default', [
    'build'
  ]);
};
