$(window).scroll(function() {

  if ($("#header.fixed").offset().top > 50) {
    $("#header").addClass("is-fixed");
  } else {
    $("#header").removeClass("is-fixed");
  }

  if ($(document).scrollTop() > 300) {
    $('#scrollTop').show();
  } else{
    $('#scrollTop').hide();
  }
});

$(function(){

  $('#typedBox').typed({
    stringsElement: $('#typed-strings'),
    loop: true,
    typeSpeed: 100
  });


  $('#toggleNavigation').click(function(){
    $('.navigation').toggleClass('active');
  });

  $('#scrollTop').click(function(e){
    e.preventDefault();
    var top = 0;
    $('body').animate({scrollTop: top}, $('body').scrollTop()/2);
    $('html').animate({scrollTop: top}, $('html').scrollTop()/2);
  });


  $('[data-scroll-target]').click(function(){
    var element = $(this).attr('data-scroll-target');
    var top = $('#' + element).offset().top - 60;
    $('body').animate({scrollTop: top}, 400);
    $('html').animate({scrollTop: top}, 400);
    $('.navigation').removeClass('active');
  });

});

